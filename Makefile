SOURCES_CUB3D =  srcs/ft_control_map.c			\
					srcs/ft_cub3d.c				\
					srcs/ft_cub3d_utils.c		\
					srcs/ft_events.c			\
					srcs/ft_fill_map.c			\
					srcs/ft_find_wall.c 		\
					srcs/ft_init.c				\
					srcs/ft_mlx.c				\
					srcs/ft_move.c				\
					srcs/ft_parse.c				\
					srcs/ft_print_wall.c		\
					srcs/ft_sprite_1.c			\
					srcs/ft_sprite_2.c			\
					srcs/ft_texture.c			\
					srcs/ft_save_bitmap.c

INCLUDE =	-Iinclude

NAME = cub3d

CC		= clang

CFLAGS	= -Wall -Wextra -Werror -g 

${NAME}:
			@make -C libft
			@make -C minilibx-linux
			${CC} ${SOURCES_CUB3D} ${CFLAGS} -Llibft -lft -Lminilibx-linux -lmlx -lXext -lX11 -lm -o ${NAME}

all: 		${NAME}

clean:
			${MAKE} clean -C libft

fclean:		clean
			${MAKE} fclean -C libft
			${MAKE} fclean -C bonus
			rm -f ${NAME}
			rm -f image.bmp

re:			fclean all

bonus:		
			@make -C libft
			@make -C minilibx-linux
			@make -C bonus

.PHONY:		all fclean clean re bonus
