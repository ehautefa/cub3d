/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_mini_map_bonus.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/19 11:13:52 by user42            #+#    #+#             */
/*   Updated: 2021/04/12 13:21:51 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_cub3d_bonus.h"

void	ft_print_mini_map(t_env *env)
{
	int		x;
	int		y;
	int		size;

	y = -1;
	size = ft_size_mini_map(env);
	while (env->map[++y])
	{
		x = -1;
		while (env->map[y][++x])
			ft_print_square(env, x, y, size);
	}
	ft_print_perso(env, size);
}

void	ft_print_square(t_env *env, int x, int y, int size)
{
	int		i;
	int		j;
	int		color;

	j = -1;
	color = ft_find_color_mini_map(env->map[y][x]);
	while (++j < size)
	{
		i = 0;
		while (i < size)
		{
			ft_mlx_pixel_put(env->mlx.img,
				x * size + 5 + i, y * size + 5 + j, color);
			i += 2;
		}
	}
}

int		ft_size_mini_map(t_env *env)
{
	float	size_x;
	float	size_y;

	if (env->size_x > (env->init.x_res / 2) ||
		env->size_y > (env->init.y_res / 2))
		return (0);
	size_x = floor(env->init.x_res / (2 * env->size_x));
	size_y = floor(env->init.y_res / (2 * env->size_y));
	if (size_x > size_y)
		return ((int)size_y);
	return ((int)size_x);
}

int		ft_find_color_mini_map(char c)
{
	if (c == '1' || c == ' ')
		return (0x000000);
	else if (c == '2')
		return (0xFFC300);
	else
		return (0xFFFFFF);
}

void	ft_print_perso(t_env *env, int size)
{
	int		i;
	int		j;
	int		size_perso;

	j = 0;
	size_perso = 4;
	while (j < size_perso)
	{
		i = 0;
		while (i < size_perso)
		{
			ft_mlx_pixel_put_sprite(env->mlx.img, env->pos.x * size + 5 + i,
					env->pos.y * size + 5 + j, 255 * 256 * 256);
			i++;
		}
		j++;
	}
}
