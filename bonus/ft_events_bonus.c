/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_events_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/19 16:02:02 by user42            #+#    #+#             */
/*   Updated: 2021/04/15 14:58:57 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_cub3d_bonus.h"

int			red_cross(int key, void *param)
{
	t_env	*env;

	env = (t_env *)param;
	exit(0);
	return (key);
}

int			key_press(int key, void *param)
{
	t_env *env;

	env = (t_env *)param;
	mlx_clear_window(env->mlx.mlx, env->mlx.win);
	if (key == 119 || key == 65362)
		env->key.w = 1;
	if (key == 115 || key == 65364)
		env->key.s = 1;
	if (key == 97)
		env->key.a = 1;
	if (key == 100)
		env->key.d = 1;
	if (key == 65361)
		env->key.l = 1;
	if (key == 65363)
		env->key.r = 1;
	if (key == 65307)
		ft_quit_mlx(env);
	key_press_bonus(key, env);
	ft_move(env);
	return (key);
}

void		key_press_bonus(int key, t_env *env)
{
	if (key == 65505)
		env->key.shift = 1;
	if (key == 122)
		env->key.z = 1;
	if (key == 109)
		env->key.m = ft_which_weapons(env->key.m);
	if (key == 110)
		env->key.n = env->key.n == 1 ? 0 : 1;
	if (key == 99)
		env->key.c = env->key.c == 1 ? 0 : 1;
}

int			key_release(int key, void *param)
{
	t_env *env;

	env = (t_env *)param;
	mlx_clear_window(env->mlx.mlx, env->mlx.win);
	if (key == 119 || key == 65362)
		env->key.w = 0;
	if (key == 115 || key == 65364)
		env->key.s = 0;
	if (key == 97)
		env->key.a = 0;
	if (key == 100)
		env->key.d = 0;
	if (key == 65361)
		env->key.l = 0;
	if (key == 65363)
		env->key.r = 0;
	if (key == 65307)
		ft_quit_mlx(env);
	if (key == 65505)
		env->key.shift = 0;
	if (key == 122)
		env->key.z = 0;
	ft_move(env);
	return (key);
}

int			resize_request(void *env)
{
	t_env	*s;
	float	*dist;

	s = (t_env *)env;
	ft_reset_opti(s->mlx.img.opti);
	ft_print_life(env);
	ft_print_weapons(s);
	if (s->key.c == 1)
		ft_print_mini_map(env);
	if (!(dist = ft_printf_wall(env)))
		ft_quit_mlx(env);
	ft_surface_color(env);
	ft_printf_sprite(env, dist);
	free(dist);
	mlx_put_image_to_window(s->mlx.mlx, s->mlx.win, s->mlx.img.img, 0, 0);
	return (0);
}
