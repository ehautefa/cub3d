/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bonus.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 10:50:05 by user42            #+#    #+#             */
/*   Updated: 2021/03/24 08:37:25 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_cub3d_bonus.h"

t_weapon	ft_weapon(int a)
{
	t_weapon	w;

	if (a == 1)
	{
		w.x_start = 00;
		w.y_start = 00;
		w.x_end = 250;
		w.y_end = 150;
	}
	else if (a == 2)
	{
		w.x_start = 00;
		w.y_start = 200;
		w.x_end = 150;
		w.y_end = 320;
	}
	else
	{
		w.x_start = 200;
		w.y_start = 200;
		w.x_end = 800;
		w.y_end = 800;
	}
	return (w);
}

t_weapon	ft_tir(int a)
{
	t_weapon	w;

	if (a == 2)
	{
		w.x_start = 150;
		w.y_start = 200;
		w.x_end = 300;
		w.y_end = 325;
	}
	else
	{
		w.x_start = 0;
		w.y_start = 0;
		w.x_end = 0;
		w.y_end = 0;
	}
	return (w);
}

void		ft_arme(t_env *env, t_weapon weapon)
{
	int		x;
	int		y;
	int		x_text;
	int		y_text;
	int		color;

	y = env->init.y_res - weapon.y_end + weapon.y_start;
	y_text = weapon.y_start;
	while (y_text < weapon.y_end && y_text >= 0 && y_text < env->tex[7].height)
	{
		x = env->init.x_res / 2;
		x_text = weapon.x_start;
		while (x_text < weapon.x_end && x_text >= 0
			&& x_text < env->tex[7].width)
		{
			color = ft_mlx_get_color(env->tex[7], x_text, y_text);
			if (color != env->tex[7].color)
				ft_mlx_pixel_put(env->mlx.img, x, y, color);
			x++;
			x_text++;
		}
		y_text++;
		y++;
	}
}

int			ft_which_weapons(int m)
{
	static int	mk;

	if (m == 0)
	{
		if (!mk || mk == 0)
			return (1);
		return (2);
	}
	if (m == 1)
		mk = 1;
	if (m == 2)
		mk = 0;
	return (0);
}

void		ft_find_sprite(float y, float x, t_env *env)
{
	int		i;

	i = -1;
	while (++i < env->sprite.nb)
	{
		if (y == env->sprite.sp[i].y && x == env->sprite.sp[i].x)
		{
			env->sprite.sp[i].id = 0;
			return ;
		}
	}
}
