/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_life_bonus.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/14 08:47:53 by user42            #+#    #+#             */
/*   Updated: 2021/04/14 08:53:36 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_cub3d_bonus.h"

void		ft_actu_life(t_env *env)
{
	if (env->map[(int)floor(env->pos.y)][(int)floor(env->pos.x)] == '3')
	{
		env->map[(int)floor(env->pos.y)][(int)floor(env->pos.x)] = '0';
		env->pos.life -= 0.3;
		ft_find_sprite(floor(env->pos.y) + 0.5, floor(env->pos.x) + 0.5, env);
	}
	if (env->map[(int)floor(env->pos.y)][(int)floor(env->pos.x)] == '4')
	{
		env->map[(int)floor(env->pos.y)][(int)floor(env->pos.x)] = '0';
		env->pos.life += 0.3;
		ft_find_sprite(floor(env->pos.y) + 0.5, floor(env->pos.x) + 0.5, env);
	}
	if (env->pos.life > 1)
		env->pos.life = 1;
	if (env->pos.life < 0)
		exit(0);
}

void		ft_print_life(t_env *env)
{
	t_weapon	l;
	int			x;
	int			y;

	l.x_start = (25 * env->init.x_res / 32);
	l.x_end = (31 * env->init.x_res / 32);
	l.y_start = 20;
	l.y_end = 50;
	y = l.y_start;
	ft_actu_life(env);
	while (++y < l.y_end)
	{
		x = l.x_start - 1;
		while (++x < l.x_end)
		{
			if (((l.x_end - x) / (float)(l.x_end - l.x_start)) < env->pos.life
			&& x < l.x_end - 2 && x > l.x_start + 1
			&& y > l.y_start + 1 && y < l.y_end - 2)
				ft_mlx_pixel_put(env->mlx.img, x, y, 0xFF0000);
			else
				ft_mlx_pixel_put(env->mlx.img, x, y, 0xFFFFFF);
		}
	}
}

int			ft_check_color(int r, int g, int b)
{
	if (r < 0 || r > 255)
		return (-1);
	if (g < 0 || g > 255)
		return (-1);
	if (b < 0 || b > 255)
		return (-1);
	return (0);
}
