/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_bonus.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/24 08:31:44 by user42            #+#    #+#             */
/*   Updated: 2021/04/15 14:57:54 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_cub3d_bonus.h"

void		ft_incr_pos_lat(t_env *env, char sign)
{
	float ang;

	if (sign == 'l')
		ang = env->pos.dir + PI / 2;
	if (sign == 'r')
		ang = env->pos.dir - PI / 2;
	env->pos.speed = 0.1;
	env->pos.x += env->pos.speed * cos(ang);
	env->pos.y -= env->pos.speed * sin(ang);
	if (env->pos.x < 1.25 || env->pos.x > env->size_x - 1.25)
		env->pos.x -= env->pos.speed * cos(ang);
	if (env->pos.y < 1.25 || env->pos.y > env->size_y - 1.25)
		env->pos.y += env->pos.speed * sin(ang);
}

float		ft_incr_ori(t_position pos, char dir, t_env *env)
{
	float	retur;
	float	incr;

	incr = PI / 32;
	if (env->key.z == 1)
		incr = PI / 16;
	if (dir == 'l')
		retur = pos.dir + incr;
	else
		retur = pos.dir - incr;
	if (retur > 2 * PI)
		retur -= 2 * PI;
	else if (retur <= 0)
		retur += 2 * PI;
	return (retur);
}

t_position	ft_incr_pos(t_env *env, int sign)
{
	env->pos.speed = 0.1;
	if (env->key.shift == 1)
		env->pos.speed = 0.2;
	env->pos.x += sign * env->pos.speed * cos(env->pos.dir);
	env->pos.y -= sign * env->pos.speed * sin(env->pos.dir);
	if (env->pos.x < 1.25 || env->pos.x > env->size_x - 1.25)
		env->pos.x -= sign * env->pos.speed * cos(env->pos.dir);
	if (env->pos.y < 1.25 || env->pos.y > env->size_y - 1.25)
		env->pos.y += sign * env->pos.speed * sin(env->pos.dir);
	return (env->pos);
}

void		ft_print_weapons(t_env *env)
{
	if (env->key.n)
		ft_arme(env, ft_tir(env->key.m));
	else if (env->key.m)
		ft_arme(env, ft_weapon(env->key.m));
}

void		ft_move(t_env *env)
{
	float	*dist;

	env->pos = env->key.w == 1 ? ft_incr_pos(env, 1) : env->pos;
	if (env->key.s)
		env->pos = ft_incr_pos(env, -1);
	if (env->key.l)
		env->pos.dir = ft_incr_ori(env->pos, 'l', env);
	if (env->key.r)
		env->pos.dir = ft_incr_ori(env->pos, 'r', env);
	if (env->key.a)
		ft_incr_pos_lat(env, 'l');
	if (env->key.d)
		ft_incr_pos_lat(env, 'r');
	ft_reset_opti(env->mlx.img.opti);
	ft_print_life(env);
	ft_print_weapons(env);
	if (env->key.c == 1)
		ft_print_mini_map(env);
	if (!(dist = ft_printf_wall(env)))
		ft_quit_mlx(env);
	ft_surface_color(env);
	ft_printf_sprite(env, dist);
	free(dist);
	mlx_put_image_to_window(env->mlx.mlx, env->mlx.win, env->mlx.img.img, 0, 0);
}
