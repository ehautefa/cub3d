/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/25 15:39:39 by ehautefa          #+#    #+#             */
/*   Updated: 2021/04/14 10:13:50 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_cub3d.h"

t_map		ft_init_map_struct(void)
{
	t_map	map;

	map.x_res = -1;
	map.y_res = -1;
	map.floor = -1;
	map.ceiling = -1;
	return (map);
}

t_position	ft_init_position(void)
{
	t_position	pos;

	pos.x = 0;
	pos.y = 0;
	pos.dir = 0;
	pos.z = 2;
	pos.mk = 0;
	return (pos);
}

char		**ft_init_map(int fd, char *file, int *size)
{
	char	**map;

	*size = ft_size_map(fd);
	if (!(map = malloc((*size + 1) * sizeof(char *))))
		return (NULL);
	ft_fill_map(file, map, *size);
	return (map);
}

t_key		ft_init_key(void)
{
	t_key	key;

	key.a = 0;
	key.d = 0;
	key.s = 0;
	key.w = 0;
	key.r = 0;
	key.l = 0;
	return (key);
}

t_wall		*ft_init_wall(void)
{
	t_wall	*wall;

	wall = malloc(sizeof(t_wall));
	wall->wall = 0;
	wall->x_wall = 0;
	wall->y_wall = 0;
	wall->orientation = 'N';
	wall->dist = 10000;
	return (wall);
}
