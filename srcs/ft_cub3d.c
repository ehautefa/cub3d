/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cub3d.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user42 <user42@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/22 07:51:27 by ehautefa          #+#    #+#             */
/*   Updated: 2021/04/14 10:29:05 by user42           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_cub3d.h"

int		main(int ac, char **av)
{
	int	save;

	save = 0;
	if (ac == 3)
		save = 1;
	if (ac != 2 && ac != 3)
	{
		printf("Error\nToo many or no arguments");
		return (-1);
	}
	if (ac == 3)
		if (ft_strcmp(av[2], "--save") != 0)
		{
			printf("Error\n2nd arguments is not correct");
			return (-1);
		}
	if (ft_strcmp(&av[1][(int)(ft_strlen(av[1]) - 4)], ".cub") != 0)
	{
		printf("Error\nFirst arguments is not a .cub");
		return (-1);
	}
	if (ft_init_struct(av[1], save) == -1)
		return (-1);
	return (0);
}

void	ft_free(t_env *env)
{
	int	i;

	i = -1;
	free(env->init.texture[0]);
	free(env->init.texture[1]);
	free(env->init.texture[2]);
	free(env->init.texture[3]);
	free(env->init.texture[4]);
	free(env->sprite.sp);
	free(env->size_line);
	while (env->map[++i])
		free(env->map[i]);
	free(env->map[i]);
	free(env->map);
	i = -1;
	while (env->mlx.img.opti[++i])
		free(env->mlx.img.opti[i]);
	free(env->mlx.img.opti);
}

int		ft_init_struct(char *file, int save)
{
	t_env		env;
	char		*error;
	int			fd;

	env.init = ft_init_map_struct();
	env.pos = ft_init_position();
	env.key = ft_init_key();
	fd = open(file, O_RDONLY);
	if (fd == -1 || read(fd, (void *)0, 0) == -1)
		return (ft_error("Error\nCan't open the map !"));
	if (!(!(error = ft_parse(&env.init, &fd))))
		return (ft_error(error));
	if (env.init.ceiling == -1 || env.init.floor == -1 || env.init.x_res == -1)
		return (ft_error("Error\nA argument is missing"));
	env.map = ft_init_map(fd, file, &env.size_y);
	env.size_x = ft_find_size_x(&env);
	if (env.size_x == -1)
		return (ft_error("Error\nMalloc"));
	error = ft_control_map(&env);
	return (ft_init_struct2(&env, save, error));
}

int		ft_find_size_x(t_env *env)
{
	int		size;
	int		i;

	i = 0;
	size = 0;
	if (!(env->size_line = malloc(sizeof(int) * env->size_y)))
		return (-1);
	while (env->map[i])
	{
		env->size_line[i] = ft_strlen(env->map[i]);
		if (env->size_line[i] > size)
			size = env->size_line[i];
		i++;
	}
	return (size);
}

int		ft_init_struct2(t_env *env, int save, char *error)
{
	if (!error)
		error = ft_sprite(env);
	if (error && error[0] == 'E')
		return (ft_error(error));
	error = ft_init_mlx(env, save);
	if (error && error[0] == 'E')
		return (ft_error(error));
	ft_free(env);
	return (0);
}
